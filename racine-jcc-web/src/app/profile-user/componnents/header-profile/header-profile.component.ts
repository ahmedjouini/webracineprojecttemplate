import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UserStoreService} from '../../../authentication/stores/user-store/user-store.service';
import {Subscription} from 'rxjs';
import {ProfileUserService} from '../../service/profile-user.service';
import {UserDto} from "../../../common/user-dto";
import {NotficationUser} from '../../../common/notficationUser';
import {MenuItem} from "primeng/api";
// test
@Component({
  selector: 'app-header-profile',
  templateUrl: './header-profile.component.html',
  styleUrls: ['./header-profile.component.scss']
})
export class HeaderProfileComponent implements OnInit {
  user: any;
  subscription?: Subscription ;
  newUserName:string='';
 lisNotification:NotficationUser[]=[];
  numberNewNotfication:number=0;
  items: MenuItem[] = [];
  currentUser:UserDto={}
  visibleSidebar1: any;
  constructor(private route:Router,
              private userStoreService: UserStoreService,
              private profileUserService:ProfileUserService ,private router:Router,  private userStore: UserStoreService) { }

  ngOnInit(): void {
    this.items = [
      {
        label: 'My Profile',
        icon: 'pi pi-fw pi-home',
        command: (click) => {
          this.visibleSidebar1=false,
          this.router.navigate(['/profile/myProfile']);
        }
      },
      {
        label: 'My Project',
        icon: 'pi pi-fw pi-list',
        items: [
          {label: 'List Project', icon: 'pi pi-fw pi-book',
            command: (click) => {
              this.visibleSidebar1=false,
              this.router.navigate(['/profile/list-project']);
            }},
          {label: 'New Project', icon: 'pi pi-fw pi-plus',
            command: (click) => {
              this.visibleSidebar1=false,
              this.router.navigate(['/profile/new-project']);
            }

          }
        ]
      },
      {
        label: 'My Documents',
        icon: 'pi pi-fw pi-file',
        items: [
          {
            label: 'List Documents',
            icon: 'pi pi-pi pi-bars',
            command: (click) => {
              this.visibleSidebar1=false,
                this.router.navigate(['/profile/list-documents']);


            }
          },
          {
            label: 'New Document',
            icon: 'pi pi-pi pi-plus',
            command: (click) => {
              this.visibleSidebar1=false,
                this.router.navigate(['/profile/new-document']);


            }
          }
        ]
      },
      {
        label: 'Return to Site ',
        icon: 'pi pi-fw pi-globe',
        command: (click) => {
          this.visibleSidebar1=false
          this.router.navigate(['/dashboard/home']);
        }
      },
      {
        label: 'Logout',
        icon: 'pi pi-fw pi-sign-out',
        command: (click) => {
          this.visibleSidebar1=false,
          this.userStore.logout();
          this.router.navigate(['']);
        }

      }
    ];
    this.user = this.userStoreService.parseJWT();

    this.profileUserService.getUser(this.user.email).subscribe(response=>{
      this.currentUser=response;


    })
    this.newUserName=this.user.name

   this.subscription = this.profileUserService.getName().subscribe(res => {

       this.currentUser.username =res;


     })
    this.getNotications();
    console.log(this.numberNewNotfication)

    }


  getNotications() {
    //test header
    console.log(this.user.id)
    this.profileUserService.getNotficationsByUser(this.user.sub).subscribe(
      (resp) => {
        this.lisNotification = resp.reverse();
        console.log(this.lisNotification);
        this.getNumberNotificationNotShowed();
      },
      (err) => {
        console.log(err);

      }
    );

  }
  getNumberNotificationNotShowed(){
    this.lisNotification.forEach(element => {
      if(element.isShowed===true){
        this.numberNewNotfication++;
      }
    });

  }

  logout() {
    this.route.navigate(['']);
    this.userStoreService.logout()  }

  clicked() {
    this.numberNewNotfication=0;

  }

  viewNotification(notification:NotficationUser,op:any,event:any) {
   if(notification.typeNotification==='has updated her profile'){
     console.log(notification)
     this.profileUserService.updateEtatNotificationToShowed(notification.id).subscribe(
       (resp) => {
         console.log(resp);
       },
       (err) => {
         console.log(err);


       }
     );
   }else{
     this.route.navigate(['/profile/etat-project',notification.idProject]);
     this.profileUserService.sendIdProjectForNotification(notification.idProject);
     if(notification.isShowed===true){
       console.log(notification.isShowed)
       this.profileUserService.updateEtatNotificationToShowed(notification.id).subscribe(
         (resp) => {
           console.log(resp);
         },
         (err) => {
           console.log(err);


         }
       );
     }

   }
    // close list comments
    op.hide(event);




  }

  goProfile() {
    this.route.navigate(['/profile/myProfile']);
  }
}


