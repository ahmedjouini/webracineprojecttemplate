import { Component, OnInit } from '@angular/core';
import {MenuItem, MessageService, PrimeIcons} from 'primeng/api';;
import {TaskDto} from '../../../common/Task';
import {ActivatedRoute, Router} from '@angular/router';
import {UserStoreService} from '../../../authentication/stores/user-store/user-store.service';
import {ProfileUserService} from '../../service/profile-user.service';
import {Project} from "../../../shared-module/model/project";
import {Subscription} from 'rxjs';
import {MailContentTask} from "../../../common/mailContentTask-dto";
import {CommentDto} from "../../../common/comment-dto";
import {environment} from "../../../../environments/environment";
import {saveAs} from "file-saver";
import {NewBackOfficeService} from "../../../new-back-office/service/new-back-office.service";


@Component({
  selector: 'app-dashboard-etata-project',
  templateUrl: './dashboard-etata-project.component.html',
  styleUrls: ['./dashboard-etata-project.component.scss']
})
export class DashboardEtataProjectComponent implements OnInit {
  events1: any[]=[];
  items: MenuItem[] = [];
  home:any;
  events2: any[]=[];

  tasks:any[]|undefined=[];
  display: boolean = false;
  displayLevel:boolean=false;
  param:number =0;
  currentProject:Project={}
  currentTasks: TaskDto[]=[];
  task:TaskDto ={};
  passedTask:TaskDto={}
  subscriptionIdProjectNotfication?: Subscription ;
  displayJob: boolean=false;
  mailContent: MailContentTask | undefined = {}
  listCommentTask: CommentDto[] = [];
  host = environment.apiBaseUrl + '/api/v1';
  displayFile: boolean=false;




  constructor(private route: ActivatedRoute,
              private userStoreService: UserStoreService,
              private profileUserService: ProfileUserService,private servicesBackOfficesService: NewBackOfficeService,
              ) { }
  ngOnInit() {


    // @ts-ignore
    this.param = this.route.snapshot.paramMap.get('id');
    this.getTasks();
    this.items = [
      {label: 'My Project '},
      {label: 'List  Project '},
      {label: 'Project '},
      {label: 'List  Tasks '},


    ];
    this.home = {icon: 'pi pi-home', routerLink: '/'};

    this.subscriptionIdProjectNotfication = this.profileUserService.getIdProjectForNotification().subscribe(res => {

      this.param =res;
      this.getProject();


    })


  }

  doJob(task:TaskDto) {
  this.passedTask=task;
  console.log( this.passedTask.type)

this.display=true
  }

  showLevel() {
    this.displayLevel=true;

  }


  getProject() {

    this.profileUserService.getProjectById(this.param).subscribe(
      (resp)=>{
        this.currentProject=resp;
      } )


  }
  getTasks() {

    this.profileUserService.getAllTasksIsNotBlockedByProject(this.param).subscribe(
      (resp) => {

        this.currentTasks=resp;
        console.log(  this.currentTasks)
// @ts-ignore
        this.currentTasks?.sort((task1,task2) => task2.id - task1.id);


        this.tasks=this.currentTasks
      }
    )
  }





  refrechListTask(event: any) {

    this.getTasks() ;
    this.display=false;
  }




  showJob(task: TaskDto) {
    this.displayJob=true
    this.task=task;
    // @ts-ignore
    this.listCommentTask=this.task.comments
   this.mailContent=this.task.mail

  }

  closeDialog() {
    this.displayJob=false;
    this.task={}
  }

  redFile() {
this.displayFile=true;

    // @ts-ignore
    this.servicesBackOfficesService.downloadFile(this.task.file.id).subscribe(
      response => {

        const file = new Blob([response], {type: 'application/octet-stream'});
        // @ts-ignore
        saveAs(file, this.task.file?.fileName);

      })

  }
}
