import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {DataService} from '../../../dashboard/service/data.service';
import {Router} from '@angular/router';
import {UserStoreService} from '../../stores/user-store/user-store.service';
import {DhashboardService} from '../../../dashboard/service/dhashboard.service';

@Component({
  selector: 'app-top-bar-authentication',
  templateUrl: './top-bar-authentication.component.html',
  styleUrls: ['./top-bar-authentication.component.css']
})
export class TopBarAuthenticationComponent implements OnInit {

  browserLang:string | null='';

  constructor(
    public translate: TranslateService,private data: DataService,
    private route :Router,
    private userStore: UserStoreService,
    private dashService: DhashboardService


  ) {

    this.browserLang =this.translate.getBrowserLang().toUpperCase()
    translate.addLangs(["EN", "FR", "AR"]);
    this.browserLang=localStorage.getItem('lang');
    if (this.browserLang !== null) {
      translate.setDefaultLang(this.browserLang)
      translate.use(this.browserLang)
    }



    }


  ngOnInit(){


  }
  changeLanguage(lang:any){
    this.translate.use(lang.target.value)
    localStorage.setItem('lang',lang.target.value);

    // window.location.reload();
    this.data.changeLanguge(lang.target.value)
    this.dashService.sendLanguage(lang.target.value);
    this.dashService.sendLanguageForService(lang.target.value);

  }

  login() {
    const user =this.userStore.parseJWT();
    if(!this.userStore.loadToken()){
      this.route.navigate(['']);

    }else{


      if(user.roles[0]==="MASTER"){
        this.route.navigate(['/backOffice/users']);
        return;
      }
      if (user.roles[0]==="ADMIN") {
        this.route.navigate(['/backOffice/admin']);
      } else {
        this.route.navigate(['/profile/myProfile']);
      }

    }


  }
}
