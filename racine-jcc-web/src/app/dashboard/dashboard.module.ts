import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import {MenubarModule} from 'primeng/menubar';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ServicesComponent } from './components/services/services.component';
import {CardModule} from 'primeng/card';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import {ToolbarModule} from "primeng/toolbar";
import { ContactComponent } from './components/mapRacine/contact.component';
import { AboutComponent } from './components/about/about.component';
import { HomeComponent } from './components/home/home.component';
import { RightsReservedComponent } from './components/footer/rights-reserved/rights-reserved.component';
import { FooterComponent } from './components/footer/footer/footer.component';
import { SocialNetworkComponent } from './components/footer/social-network/social-network.component';
import { AddressesComponent } from './components/footer/addresses/addresses.component';
import { ButtonModule } from 'primeng/button';
import {SlideMenuModule} from "primeng/slidemenu";
import {DialogModule} from "primeng/dialog";
import {MessageModule} from "primeng/message";
import {KeyFilterModule} from "primeng/keyfilter";
import {OverlayPanelModule} from 'primeng/overlaypanel';
import { Middle2Component } from './components/middle2/middle2.component';
import { Middle4Component } from './components/middle4/middle4.component';
import { Middle6Component } from './components/middle6/middle6.component';

import { CardComponent } from './components/card/card.component';
import {SplitButtonModule} from "primeng/splitbutton";
import {MarqueeComponent} from "./components/marquee/marquee.component";
import {QuotationComponent} from "./components/quotation/quotation.component";
import {GalleriaModule} from "primeng/galleria";
import {ScrollPanelModule} from "primeng/scrollpanel";

import { MapsRacineComponent } from './components/maps-racine/maps-racine.component';
import { ContactRacineComponent } from './components/contact-racine/contact-racine.component';
import {CascadeSelectModule} from "primeng/cascadeselect";
import {DropdownModule} from "primeng/dropdown";


export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json')
}







@NgModule({
  declarations: [
    DashboardComponent,
    ServicesComponent,
    TopBarComponent,
    RightsReservedComponent,
    FooterComponent,
    SocialNetworkComponent,
    AddressesComponent,
    ContactComponent,
    AboutComponent,
    HomeComponent,
    Middle2Component,
    Middle4Component,
    Middle6Component,
    CardComponent,
    MarqueeComponent,
    QuotationComponent,
    MapsRacineComponent,
    ContactRacineComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FormsModule,
    MenubarModule,
    CardModule,
    ButtonModule,
    SlideMenuModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ToolbarModule,
    SlideMenuModule,
    DialogModule,
    ReactiveFormsModule,
    MessageModule,
    KeyFilterModule,
    OverlayPanelModule,
    SplitButtonModule,
    GalleriaModule,
    CascadeSelectModule,
    DropdownModule


  ],
  exports: [
    TopBarComponent,
    RightsReservedComponent
  ]


})
export class DashboardModule { }
