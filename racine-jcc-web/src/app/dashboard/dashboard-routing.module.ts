import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/mapRacine/contact.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ServicesComponent } from './components/services/services.component';
import {HomeComponent} from "./components/home/home.component";
import {Middle4Component} from './components/middle4/middle4.component';
import {ConfirmByPhoneNumberComponent} from '../authentication/component/confirm-by-phone-number/confirm-by-phone-number.component';
import {QuotationComponent} from './components/quotation/quotation.component';


const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'new-service-master',
        component: ServicesComponent,

      },
      {
        path: 'about',
        component: AboutComponent,
      },
     {
        path: 'mapRacine',
        component: ContactComponent,
      },
      {
        path: 'home',
        component: HomeComponent,
      },
      {
        path: 'test',
        component: Middle4Component,
      },

      { path: 'services/:id', component:QuotationComponent  },

    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
