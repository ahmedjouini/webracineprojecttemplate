import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Service} from '../../model/service';
import {Meta, Title} from '@angular/platform-browser';
import {GlobalConstants} from '../../../common/constant/GlobalConstants';
import {MetaServiceService} from "../../service/meta-service.service";
import {Router} from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  lang: any;
  listServices: Service[] = [];
  image_home:string=GlobalConstants.IMAGE_HOME;
  alt_image_Home:string=GlobalConstants.ALT_IMAGE_HOME


  constructor(public translate: TranslateService,
              private title :Title,private meta: Meta,private metaService: MetaServiceService,
              private router:Router) {
    this.lang = localStorage.getItem('lang') || {};
  }

  ngOnInit(): void {
    this.meta.updateTag({ name: 'description', content: GlobalConstants.Dashbord })
    this.meta.updateTag({ name: 'keywords', content: GlobalConstants.KeyWordsHome})
    this.metaService.setCanonicalURL();

  }

}
