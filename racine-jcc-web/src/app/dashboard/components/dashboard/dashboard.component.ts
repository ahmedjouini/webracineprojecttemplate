import {Component, HostListener, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import {UserStoreService} from "../../../authentication/stores/user-store/user-store.service";
import {Title} from '@angular/platform-browser';
import {GlobalConstants} from '../../../common/constant/GlobalConstants';
import {MetaServiceService} from "../../service/meta-service.service";
import {Subscription} from 'rxjs';
import {DhashboardService} from '../../service/dhashboard.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  selectLang:any;
  subscription?: Subscription ;
  @HostListener("window:scroll", ['$event']) onWindowScroll(e:any) {

    let a = document.getElementById('button');


    if (e.target['scrollingElement'].scrollTop > 300) {
      // @ts-ignore
      a.classList.add("show");

    }else{
      // @ts-ignore
      a.classList.remove("show");

    }


  }
  constructor(private title :Title,private router: Router, private userStoreService: UserStoreService ,private metaService: MetaServiceService,private dashboardService:DhashboardService ) {
    this.selectLang=localStorage.getItem('lang');
      document.documentElement.lang=this.selectLang;
  }
  ngOnInit() {
    this.subscription = this.dashboardService.getLanguage().subscribe(res => {
      this.selectLang =res;


    })
   this. userStoreService.parseJWT()
   // this.title.setTitle(GlobalConstants.Contact)
    this.metaService.setCanonicalURL();
  //  this.router.navigate(['/home']);



  }
  public buttonClick(fragment: string): void {
    this.router.navigateByUrl('#' + fragment);

  }
  toTop() {
    // @ts-ignore
    document.getElementById('header').scrollIntoView({behavior :'smooth'});

  }
}



