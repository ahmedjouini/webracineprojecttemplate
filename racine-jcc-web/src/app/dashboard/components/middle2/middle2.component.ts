import { Component, OnInit } from '@angular/core';
import {MenuItem} from "primeng/api";
import {Subscription} from 'rxjs';
import {DhashboardService} from '../../service/dhashboard.service';

@Component({
  selector: 'app-middle2',
  templateUrl: './middle2.component.html',
  styleUrls: ['./middle2.component.css']
})
export class Middle2Component implements OnInit {
  subscription?: Subscription;
  selectLang:any ="EN"
  constructor(private dashboardService:DhashboardService) {
    this.selectLang = localStorage.getItem('lang');
  }
  ngOnInit(): void {

    this.subscription = this.dashboardService.getLanguage().subscribe(res => {
      this.selectLang =res;
    })

  }

}
