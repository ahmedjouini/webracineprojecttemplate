import { Component, OnInit } from '@angular/core';
import {Subscription} from 'rxjs';
import {DhashboardService} from '../../service/dhashboard.service';

@Component({
  selector: 'app-contact-racine',
  templateUrl: './contact-racine.component.html',
  styleUrls: ['./contact-racine.component.scss']
})
export class ContactRacineComponent implements OnInit {
  subscription?: Subscription ;

  selectLang:any ="EN"

  constructor(private dashboardService:DhashboardService) {
    this.selectLang=localStorage.getItem('lang');
  }

  ngOnInit(): void {

    this.subscription = this.dashboardService.getLanguage().subscribe(res => {
      this.selectLang =res;


  })

}
}
