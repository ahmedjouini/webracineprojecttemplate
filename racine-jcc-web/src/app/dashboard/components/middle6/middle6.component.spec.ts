import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Middle6Component } from './middle6.component';

describe('Middle6Component', () => {
  let component: Middle6Component;
  let fixture: ComponentFixture<Middle6Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Middle6Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Middle6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
