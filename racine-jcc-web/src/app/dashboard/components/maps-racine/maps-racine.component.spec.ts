import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapsRacineComponent } from './maps-racine.component';

describe('MapsRacineComponent', () => {
  let component: MapsRacineComponent;
  let fixture: ComponentFixture<MapsRacineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapsRacineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapsRacineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
