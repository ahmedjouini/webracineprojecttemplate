import { Component, OnInit } from '@angular/core';
//import { Loader } from '@googlemaps/js-api-loader';
import {contentMarker1, contentMarker2, contentMarker3, contentMarker4, contentMarker5, contentMarker6} from '../../TestData/Map-Test-Data';

@Component({
  selector: 'app-maps-racine',
  templateUrl: './maps-racine.component.html',
  styleUrls: ['./maps-racine.component.css']
})
export class MapsRacineComponent implements OnInit {
  title = 'google-maps';
  image = '../../../../assets/images/marker/marker.png';
  infoWindows = [];


  constructor() {
  }

  ngOnInit(): void {
  /*  let loader = new Loader({
      apiKey: 'AIzaSyCN3Q6zCjywCkPKy_Vl3ZZuf5OrZ76YJkE',

    })*/

    /*  loader.load().then(() => {
        const center_map = { lat:  31.460752755796126, lng: 	21.476952461104073 }


        const map = new google.maps.Map(document.getElementById("map") as HTMLElement, {
          center: center_map,
          zoom: 5,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
        })
        const list_marker:[number, number, number, string,string][] =[
          [
            25.28850743074894,
            51.528261750422146 ,
            google.maps.Animation.BOUNCE,
            this.image,
            contentMarker1
          ],
          [
            24.497010473903867,
            54.48598629119674,
            google.maps.Animation.BOUNCE,
            this.image,
            contentMarker2
          ],
          [
            36.33110103339664,
            9.29748855817844,
            google.maps.Animation.BOUNCE,
            this.image,
            contentMarker3
          ],[
            29.903196768717688,
            31.046276896962922 ,
            google.maps.Animation.BOUNCE,
            this.image,
            contentMarker4
          ],[
            24.98739596626348,
            39.32699628668064,
            google.maps.Animation.BOUNCE,
            this.image,
            contentMarker5
          ],[
            33.95425579677485,
            -6.864400992973484,
            google.maps.Animation.BOUNCE,
            this.image,
            contentMarker6
          ]
        ];
        for (let i = 0; i < list_marker.length; i++) {
          const current_marker = list_marker[i];
          const mark =new google.maps.Marker({
            position: { lat: current_marker[0], lng: current_marker[1] },
            map,
            icon: current_marker[3],
            animation:current_marker[2],

          });
          const infowindow = new google.maps.InfoWindow({
            content: current_marker[4],
          });
          // @ts-ignore
          this.infoWindows.push(infowindow);
          mark.addListener("click", () => {
            this.closeAllInfoWindows();


            infowindow.open({
              // @ts-ignore
              anchor: mark,
              map,
              shouldFocus: false,
            });
          });
        }
      })
    }

    closeAllInfoWindows() {
      for (var i=0;i<this.infoWindows.length;i++) {
        // @ts-ignore
        this.infoWindows[i].close();
      }
    }*/
  }
}
