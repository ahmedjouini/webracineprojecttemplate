import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {NewBackOfficeService} from '../../../new-back-office/service/new-back-office.service';
import {Service} from '../../model/service';
import {environment} from '../../../../environments/environment';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-quotation',
  templateUrl: './quotation.component.html',
  styleUrls: ['./quotation.component.scss']
})
export class QuotationComponent implements OnInit {
  host = environment.apiBaseUrl + '/api/v1';
  id: any;
  service:Service={};
  lang:any;
  constructor(public translate: TranslateService,private route: ActivatedRoute,private newBackOfficesService: NewBackOfficeService,) {

    this.translate.use(<string>localStorage.getItem('lang'))


  }
  ngOnInit(): void {
    window.scroll(0,0);
    this.id = this.route.snapshot.paramMap.get('id');
    this.lang = localStorage.getItem("lang") || {};

      this.newBackOfficesService.getService(this.id).subscribe(response=>{
        this.service=response

      })


  }

}
