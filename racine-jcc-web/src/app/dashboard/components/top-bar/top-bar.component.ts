import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {Subscription} from "rxjs";
import {DataService} from "../../service/data.service";
import {Router} from '@angular/router';
import {UserStoreService} from '../../../authentication/stores/user-store/user-store.service';
import {DhashboardService} from "../../service/dhashboard.service";

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent implements OnInit {




  constructor(
    public translate: TranslateService,private data: DataService,
    private route :Router,
    private userStore: UserStoreService,
    private dashService: DhashboardService


  ) {

    translate.addLangs(["EN", "FR","AR"]);
    translate.setDefaultLang("EN");

    const browserLang = translate.getBrowserLang();
 // translate.use(browserLang.match(/en|fr/) ? browserLang : "en");
  }
  ngOnInit(){
    console.log(this.translate.getLangs())


  }
  changeLanguage(lang:any){
    this.translate.use(lang.target.value)
    localStorage.setItem('lang',lang.target.value);

   // window.location.reload();
    this.data.changeLanguge(lang.target.value)
    this.dashService.sendLanguage(lang.target.value)


  }

  login() {
    const user =this.userStore.parseJWT();
    if(!this.userStore.loadToken()){
      this.route.navigate(['']);

    }else{
      if(user.roles[0]==="MASTER"){
        this.route.navigate(['/new-back-office/dashboard/list-project']);
        return;
      }
      if (user.roles[0]==="ADMIN") {
        this.route.navigate(['/new-back-office/dashboard/list-project']);
      } else {
        this.route.navigate(['/profile/myProfile']);
      }

    }


  }
}
