import {StatmentOfWork} from "./statmentOfWork";
import {UserDto} from '../../common/user-dto';
import {TaskDto} from "../../common/Task";

export interface Project {
  id?: number;
  name?: string;
  description?: string;
  createdDate?:Date;
  nameService?: string;
  appStatementOfWork?:StatmentOfWork;
  nameAdmin?: string;
  nameCustomer?:string;
  tasks?:TaskDto[];
 /// idProject:number;
}
