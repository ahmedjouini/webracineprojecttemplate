import { Injectable } from '@angular/core';
import {UserDto} from '../../common/user-dto';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpEvent, HttpHeaders, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Service} from "../../shared-module/model/service";
import {Project} from "../../shared-module/model/project";
import {StatmentOfWork} from "../../shared-module/model/statmentOfWork";
import {CommentDto} from "../../common/comment-dto";
import {TaskDto} from "../../common/Task";
import {MailContentTask} from "../../common/mailContentTask-dto";
import {UserStoreService} from "../../authentication/stores/user-store/user-store.service";
import {NotficationUser} from "../../common/notficationUser";

@Injectable({
  providedIn: 'root'
})
export class NewBackOfficeService {


  constructor(private http: HttpClient,private userStoreService:UserStoreService) { }

  register(user:UserDto){

    return this.http.post(environment.apiBaseUrl + `/register`,user, {
      observe: "response",
    });
  }
  getHeadres() {
    return   new HttpHeaders({authorization:'Bearer '+this.userStoreService.getToken()});
  }
  ////////////////////////////////////
  createCustomerByAdmin(user: UserDto, idUser?: string) {
    if(this.userStoreService.getToken()===null){      return this.http.post(environment.apiBaseUrl + `/api/v1/new-user/${idUser}`, user)
    }else {
      return this.http.post(environment.apiBaseUrl + `/api/v1/new-user/${idUser}`, user, {headers:this.getHeadres()}
    );
    }
  }
  getAllUsers(idUser?: string) {
    if(this.userStoreService.getToken()===null) {   return this.http.get<UserDto[]>(environment.apiBaseUrl + `/api/v1/all-users/${idUser}`);
    }else{
    return this.http.get<UserDto[]>(environment.apiBaseUrl + `/api/v1/all-users/${idUser}`, {headers:this.getHeadres()});
    }
  }  getUsers() {
  if(this.userStoreService.getToken()===null) {

    return this.http.get<UserDto[]>(environment.apiBaseUrl + `/api/v1/users`);
  }else{
    return this.http.get<UserDto[]>(environment.apiBaseUrl + `/api/v1/users`, {headers: this.getHeadres()});
  }
  }
  getAdmins() {
if(this.userStoreService.getToken()===null){
  return this.http.get<UserDto[]>(environment.apiBaseUrl + `/api/v1/admins-without-master` )
}else{
    return this.http.get<UserDto[]>(environment.apiBaseUrl + `/api/v1/admins-without-master`, {headers:this.getHeadres()});
}
  }

  enableAccountUser(id: string): Observable<any> {
    if(this.userStoreService.getToken()===null){
    return this.http.put(environment.apiBaseUrl + `/api/v1/approve-account-user`, id);
  }else{
      return this.http.put(environment.apiBaseUrl + `/api/v1/approve-account-user`, id,  {headers:this.getHeadres()});

    }
  }


deleteProjectFomList(idProject: number,idUser: string) {
  if(this.userStoreService.getToken()!==null) {
    return this.http.delete<string>(environment.apiBaseUrl + `/api/v1/delete-project-from-liste/${idProject}/${idUser}`, {headers: this.getHeadres()})
  }else {
    return this.http.delete<string>(environment.apiBaseUrl + `/api/v1/delete-project-from-liste/${idProject}/${idUser}`)

  }
}

getUser(idUser?: string) {
  if(this.userStoreService.getToken()!==null) {
    return this.http.get<UserDto>(environment.apiBaseUrl + `/api/v1/user/${idUser}`, {headers: this.getHeadres()});
  }else{
    return this.http.get<UserDto>(environment.apiBaseUrl + `/api/v1/user/${idUser}`);

  }
}
  getAllCustomers() {
    if(this.userStoreService.getToken()===null) {
      return this.http.get<UserDto[]>(environment.apiBaseUrl + `/api/v1/all-customers`);
    }else{
      return this.http.get<UserDto[]>(environment.apiBaseUrl + `/api/v1/all-customers`, {headers: this.getHeadres()});

    }
  }

  getAllMaster() {
    if(this.userStoreService.getToken()!==null) {
      return this.http.get<UserDto[]>(environment.apiBaseUrl + `/api/v1/all-master`, {headers: this.getHeadres()});
    }else{
      return this.http.get<UserDto[]>(environment.apiBaseUrl + `/api/v1/all-master`);
    }
  }
  createNewProjectByAdmin ( idAdmin: string | undefined, idCustomer: string | undefined, project: Project) {

    return this.http.post(environment.apiBaseUrl + `/api/v1/new-project-admin/${idAdmin}/${idCustomer}`, project, {headers:this.getHeadres()})
  }

  createNewProjectByMaster ( idMaster:string,idAdmin: string | undefined, idCustomer: string | undefined, project: Project) {

    return this.http.post(environment.apiBaseUrl + `/api/v1/new-project-master/${idMaster}/${idAdmin}/${idCustomer}`, project, {headers:this.getHeadres()})
  }

// test t

  /************************service***********************/
  uploadPhotoServices(service: Service): Observable<HttpEvent<{}>> {
    // @ts-ignore
    const req = new HttpRequest('POST', environment.apiBaseUrl + '/uploadPhoto', service, {
      reportProgress: true,
      responseType: 'text',
    });

    return this.http.request(req)
  }


  SaveService(fromData: FormData): Observable<any> {
    return this.http.post(environment.apiBaseUrl + `/api/v1/SaveService`, fromData, {headers:this.getHeadres()})
  }



  deleteUser(idUser: string) {

    if(this.userStoreService.getToken()!==null) {
    return this.http.delete<string>(environment.apiBaseUrl + `/api/v1/deleteAccountUser/${idUser}`, {headers:this.getHeadres()})
    }else{
      return this.http.delete<string>(environment.apiBaseUrl + `/api/v1/deleteAccountUser/${idUser}`)

    }
  }

  deleteUserByAdmin(idUser?: string,idAdmin?: string){
    if(this.userStoreService.getToken()!==null) {
      return this.http.delete<string>(environment.apiBaseUrl + `/api/v1/delete-user-by-admin/${idUser}/${idAdmin}`, {headers: this.getHeadres()})
    }else{
      return this.http.delete<string>(environment.apiBaseUrl + `/api/v1/delete-user-by-admin/${idUser}/${idAdmin}`)

    }
  }

  SaveFolder(fromData: FormData) {
    return this.http.post(environment.apiBaseUrl +`/api/v1/uploadFiles`, fromData, {headers:this.getHeadres()})
  }

  listFiles(): Observable<StatmentOfWork[]> {
    return this.http.get<StatmentOfWork[]>(environment.apiBaseUrl +`/files`
    );
  }

  downloadFolder(id: number): Observable<Blob> {

    // @ts-ignore
    return this.http.get(environment.apiBaseUrl + `/api/v1/downloadFile/${id}`, {responseType:'blob'});
  }

  getAllServices() {
    return this.http.get<Service[]>(environment.apiBaseUrl + `/api/v1/services/all`);

  }
  getService(id: number) {
    return this.http.get<Service>(environment.apiBaseUrl + `/api/v1/getService/${id}`);

  }

  getFile(id: number) {

    return this.http.get(environment.apiBaseUrl + `/api/v1/statementOfWorkService/${id}`, {headers:this.getHeadres()}
    );
  }

  deleteService(id: number) {
    return this.http.delete<string>(environment.apiBaseUrl + `/api/v1/delete-service/${id}`, {headers:this.getHeadres()})

  }
  getAllProjectsWithoutAdmin() {

    return this.http.get<Project[]>(environment.apiBaseUrl + `/api/v1/projects-Without-admin`, {headers:this.getHeadres()});

  }
  addProjecToAdmin(idAdmin?: string, idProject?: number){

    return this.http.put(environment.apiBaseUrl + `/api/v1/add-project/${idAdmin}/${idProject}`,{
      observe: 'response',
    }, {headers:this.getHeadres()})
  }



addAdminToMaster(idAdmin?: string, idMaster?: string){
  if(this.userStoreService.getToken()!==null) {
  return this.http.put(environment.apiBaseUrl + `/api/v1/add-admin-to-master/${idAdmin}/${idMaster}`,{
    observe: 'response',
  }, {headers:this.getHeadres()})}else{
    return this.http.put(environment.apiBaseUrl + `/api/v1/add-admin-to-master/${idAdmin}/${idMaster}`,{
      observe: 'response',})
    }

}
/////task//////////////////

  // tasks
  getAllTasksByProject(id:number){
    if(this.userStoreService.getToken()!==null) {
      return this.http.get<TaskDto[]>(environment.apiBaseUrl + `/api/v1/getAllTaskProject/${id}`, {headers: this.getHeadres()});
    }else {
      return this.http.get<TaskDto[]>(environment.apiBaseUrl + `/api/v1/getAllTaskProject/${id}`);
    }
  }
  addTaskToProject(task:TaskDto,idProject:number,idEmmitor:string) {
    if(this.userStoreService.getToken()!==null) {
      return this.http.post(environment.apiBaseUrl +`/api/v1/addTaskToProject/${idProject}/${idEmmitor}`, task, {headers: this.getHeadres()})
    }else {
      // @ts-ignore
      return this.http.post(environment.apiBaseUrl +`/api/v1/addTaskToProject/${idProject}/${idEmmitor}`)

    }
  }
  enableTaskForUser(id:String,idEmmitor:string,idProject:number){
    if(this.userStoreService.getToken()!==null) {
    return this.http.put(environment.apiBaseUrl +`/api/v1/enableTaskForUser/${idEmmitor}/${idProject}`,id,{headers: this.getHeadres()})}
    else{
      // @ts-ignore
      return this.http.put(environment.apiBaseUrl +`/api/v1/enableTaskForUser/${idEmmitor}`,id)
    }
  }
  deleteTask(idTask:string,idProduct:number){
    if(this.userStoreService.getToken()!==null) {
      return this.http.delete<string>(environment.apiBaseUrl +`/api/v1/deleteTask/${idTask}/${idProduct}`, {headers: this.getHeadres()})
    }else{
      return this.http.delete<string>(environment.apiBaseUrl +`/api/v1/deleteTask/${idTask}/${idProduct}`)

    }
  }

  getAllCommentsByTasks(id:string){
    if(this.userStoreService.getToken()!==null) {
      return this.http.get<CommentDto[]>(environment.apiBaseUrl + `/api/v1/getAllCommentByTaskProject/${id}`, {headers: this.getHeadres()});
    }else{
      return this.http.get<CommentDto[]>(environment.apiBaseUrl + `/api/v1/getAllCommentByTaskProject/${id}`);

    }
  }
  getMailContentBytask(idTask: string) {
    if(this.userStoreService.getToken()!==null) {
    return this.http.get<MailContentTask>(
      environment.apiBaseUrl + `/api/v1/getMailContentBytask/${idTask}`, {headers:this.getHeadres()});}else{
      return this.http.get<MailContentTask>(
        environment.apiBaseUrl + `/api/v1/getMailContentBytask/${idTask}`);
    }
  }


  downloadFile(id:number):Observable<Blob> {

    return this.http.get(environment.apiBaseUrl + `/api/v1/downloadFileTask/${id}`,{responseType:'blob'});
  }

  deleteProject(idProject:any){
    if(this.userStoreService.getToken()!==null) {
    return this.http.delete(environment.apiBaseUrl +`/api/v1/delete-project/${idProject}`,
      {headers:this.getHeadres()})}else{
      return this.http.delete(environment.apiBaseUrl +`/api/v1/delete-project/${idProject}`
        )
    }
  }



  sendNotfication(idUser1: string | undefined, idUser2: string | undefined, notification: NotficationUser) {
    return this.http.post(environment.apiBaseUrl + `/api/v1/send-notfications-users/${idUser1}/${idUser2}`, notification)
  }

  getProject(idProject: number) {
    if(this.userStoreService.getToken()!==null) {
      return this.http.get<UserDto>(environment.apiBaseUrl + `/api/v1/project/${idProject}`, {headers: this.getHeadres()});
    }else{
      return this.http.get<UserDto>(environment.apiBaseUrl + `/api/v1/project/${idProject}`);

    }
  }


  sendNotficationForApproved(notification:NotficationUser,idUser:string) {
    return this.http.post(environment.apiBaseUrl + `/api/v1/approve-update-profile-customer/${idUser}`, notification)
  }

}
