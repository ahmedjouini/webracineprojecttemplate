import {Component, OnInit} from '@angular/core';
import {Project} from "../../../shared-module/model/project";
import {UserDto} from "../../../common/user-dto";
import {MenuItem} from "primeng/api";

import {UserStoreService} from "../../../authentication/stores/user-store/user-store.service";
import {Router} from "@angular/router";
import {Table} from "primeng/table";
import {NewBackOfficeService} from "../../service/new-back-office.service";

@Component({
  selector: 'app-list-project',
  templateUrl: './list-project.component.html',
  styleUrls: ['./list-project.component.css']
})
export class ListProjectComponent implements OnInit {

  display: boolean = false;
  displayEnable: boolean = false;
  currentIdProject=0;
  listProject: Project[] = [];
  first = 0;

  rows = 5;
  items: MenuItem[] = [];

  home: any;

  user: any;

users:UserDto[]=[];
  deleteAdmin: boolean=false;
  constructor( private newBackOfficesService: NewBackOfficeService, private userStoreService: UserStoreService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.user = this.userStoreService.parseJWT()
    this.items = [
      {label: 'Project Management'},
      {label: 'List Projects With Admin'},

    ];

    this.home = {icon: 'pi pi-home', routerLink: '/'};

    this.getAllProjects()

  }

  getAllProjects() {
    this.listProject=[]
    if (this.user.roles[0] === 'MASTER') {
      this.newBackOfficesService.getAllUsers(this.user.sub).subscribe((response) => {
        this.users=response;
        this.users.forEach(admin => {
          admin.projects?.forEach(project => {
            this.listProject.push(project)
          })
        });

      })
    } else
      this.newBackOfficesService.getUser(this.user.sub).subscribe((response) => {
        console.log(response)
        // @ts-ignore

        this.listProject = response.projects;
      })
  }
  popupDeleteProject(idProject: number){
    this.display=true
    this.currentIdProject=idProject;
  }

  deleteProjectFomList(){
    this.newBackOfficesService.deleteProjectFomList( this.currentIdProject,this.user.sub).subscribe(resp=>{

    },error => {
        this. getAllProjects()
    }
    )
    this.deleteAdmin=false;

  }
  deleteProject(){
    this.newBackOfficesService.deleteProject( this.currentIdProject).subscribe(resp=>{

      },error => {
        this. getAllProjects()
      }
    )
    this.display=false;

  }





  next() {
    this.first = this.first + this.rows;
  }

  prev() {
    this.first = this.first - this.rows;
  }

  reset() {
    this.first = 0;
  }

  isLastPage(): boolean {
    return this.listProject
      ? this.first === this.listProject.length - this.rows
      : true;
  }

  isFirstPage(): boolean {
    return this.listProject ? this.first === 0 : true;
  }







  clear(table: Table) {
    table.clear();
  }

  goDetail(id:number) {
    this.router.navigate(['/new-back-office/dashboard/update-project/',id]);

  }

  popupDeleteAdminOfProject(id:number) {

    this.currentIdProject=id;
    this.deleteAdmin=true;
  }
}
