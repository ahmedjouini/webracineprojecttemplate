import { Component, OnInit } from '@angular/core';
import {Service} from "../../../dashboard/model/service";
import {UserDto} from "../../../common/user-dto";
import {MenuItem, MessageService, PrimeNGConfig} from "primeng/api";
import {Project} from "../../../shared-module/model/project";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserStoreService} from "../../../authentication/stores/user-store/user-store.service";
import {ProfileUserService} from "../../../profile-user/service/profile-user.service";
import {Router} from "@angular/router";
import {saveAs} from "file-saver";
import {NewBackOfficeService} from "../../service/new-back-office.service";
import {NotficationUser} from '../../../common/notficationUser';
import {MessageConstants} from '../../../common/constant/message';

@Component({
  selector: 'app-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.css']
})
export class NewProjectComponent implements OnInit {

  services: Service[] = [];
  users: UserDto[] = []
  listAdmin: UserDto[] = []
  items: MenuItem[] = [];
  project: Project = {}
  namesServices: string[] = []
  home: any;

 user: any;
  serviceList: Service = {}
  customer: UserDto = {}
  admin: UserDto = {}
  isDisabledStartProject = false;
  isDownload = false;
  formNewProject = new FormGroup({
    projectname: new FormControl('', Validators.required),
    calendar: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    service: new FormControl('', Validators.required),
    user: new FormControl('', Validators.required),
    admin: new FormControl('', Validators.required),
  });
  private errorMessages: string = '';
  display: boolean = false;
  displayBasic: boolean = false;
  notfication:NotficationUser={};

  constructor(private newBackOfficesService: NewBackOfficeService,
              private userStoreService: UserStoreService,
              private profileUserService: ProfileUserService,
              private primengConfig: PrimeNGConfig,
              private router: Router,private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.user = this.userStoreService.parseJWT();

    this.items = [
      {label: 'My Project '},
      {label: 'New Project'},

    ];

    this.home = {icon: 'pi pi-home', routerLink: '/'};


    this.getAllServices();
    this.getAllCustomers();
    this.getAllAdmin();
  }

  getAllServices() {
    this.newBackOfficesService.getAllServices().subscribe(
      response => {
        this.services = response;
        // this.namesServices=this.services.


      }, error => {

      }
    )


  }

  get f() {
if(this.user.roles[0]==='MASTER'){
    if (!this.formNewProject.invalid && this.isDownload) {
      this.isDisabledStartProject = true;
    } else {
      this.isDisabledStartProject = false;
    }
    return this.formNewProject.controls
}else{
  if ( this.isDownload) {
    this.isDisabledStartProject = true;
  } else {
    this.isDisabledStartProject = false;
  }
  return this.formNewProject.controls
    }
  }

  getAllCustomers() {
    this.newBackOfficesService.getAllCustomers().subscribe(
      response => {
        this.users = response;


        console.log(this.users)


      }, error => {
        console.log("this.users")
      }
    )


  }

  getAllAdmin() {
    this.newBackOfficesService.getAllUsers(this.user.sub).subscribe((response) => {
      this.listAdmin = response;
      console.log( this.listAdmin)

    });
    this.display=false;



  }

  startProject() {
    if (this.serviceList.name) {
      // @ts-ignore
      this.project.nameService = this.serviceList.name;
      this.isDownload = true;
      if(this.user.roles[0]==='ADMIN'){
        console.log('Admin')
        this.createNewProjectByAdmin();
      }else if(this.user.roles[0]==='MASTER'){
        console.log('Master')

        this.createNewProjectByMaster();
      }

    }
    this.displayBasic = true;
  }


  createNewProjectByAdmin(){
    this.newBackOfficesService.createNewProjectByAdmin( this.user.sub,this.customer.id, this.project).subscribe(
      response => {
        console.log(response)
        setTimeout(()=>{
          this.showSuccess()

        }, 5000);
        setTimeout(()=>{
          this.router.navigate(['/new-back-office/dashboard/list-project']);

        }, 1500);

        //


      }, error => {

        console.log(error)
      }
    )
  }
  // test h
  createNewProjectByMaster(){
    console.log(this.admin)
    this.newBackOfficesService.createNewProjectByMaster( this.user.sub,this.admin.id,this.customer.id, this.project).subscribe(
      response => {
        console.log(response)
        setTimeout(()=>{
          this.showSuccess()

        }, 5000);
        setTimeout(()=>{
          this.router.navigate(['/new-back-office/dashboard/list-project']);

        }, 1500);

        //


      }, error => {

        console.log(error)
      }
    )
  }
  download(id: any, fileName: any) {
    this.isDownload = true;
    this.display = false;

    this.newBackOfficesService.downloadFolder(id).subscribe(
      response => {

        const file = new Blob([response], {type: 'application/octet-stream'});
        saveAs(file, fileName);

      })

  }

  cancel() {
    this.router.navigateByUrl('/profile/list-project', {skipLocationChange: true}).then(() => {
      this.router.navigate(['/profile/new-project']);
    });
  }


  displayPoupup() {
    this.display = true;

  }


  showSuccess() {
    this.messageService.add({severity:'success', summary: 'Success', detail: 'good job'});
  }

  sendNotficationToUsers(name: string | undefined) {
    this.notfication.message=MessageConstants.MESSAGE1;
    this.notfication.isShowed=true;
    this.notfication.name=name;
    this.notfication.createdDate=new Date();
    this.newBackOfficesService.sendNotfication(this.admin.id,this.customer.id,this.notfication).subscribe((response) => {
      console.log(response)
    });
    this.display=false;



  }
}
