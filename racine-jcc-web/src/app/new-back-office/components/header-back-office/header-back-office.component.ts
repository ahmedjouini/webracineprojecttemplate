import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserStoreService} from '../../../authentication/stores/user-store/user-store.service';
import {ProfileUserService} from '../../../profile-user/service/profile-user.service';
import {NotficationUser} from '../../../common/notficationUser';
import {Project} from "../../../shared-module/model/project";
import {NewBackOfficeService} from '../../service/new-back-office.service';
import {UserDto} from '../../../common/user-dto';

class notification {
  constructor(message: string, createdOn: Date, createdBy: number) {
    this.message = message;
    this.createdBy = createdBy;
    this.createdOn = createdOn;
  }
  message: string;
  createdOn: Date;
  createdBy: number;
}

@Component({
  selector: 'app-header-back-office',
  templateUrl: './header-back-office.component.html',
  styleUrls: ['./header-back-office.component.css']
})
export class HeaderBackOfficeComponent implements OnInit {
  @Input() user: any
  nameUser = ''
  notifications: notification[] | undefined;
  lisNotification: NotficationUser[] = [];
  numberNewNotfication: number = 0;
  param: string | null = '';
  project: Project = {}
  updateUser: UserDto = {}


  constructor(private router: Router,
              private userStoreService: UserStoreService,
              private profileUserService: ProfileUserService,
              private newBackOfficesService: NewBackOfficeService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    if (this.user === null) {
      this.nameUser = 'super Master'
    } else {


      if (this.user.roles[0] === 'MASTER') {
        this.nameUser = 'Master Profile'

      } else if (this.user.roles[0] === 'ADMIN') {
        this.nameUser = 'Admin Profile'
      }
    }


    this.notifications = [];
    for (var i = 1; i <= 5; i++) {
      var notificationObj = new notification("Message " + i, new Date(), 1)
      // @ts-ignore
      this.notifications.push(notificationObj);
    }
    this.getNotications();
  }


  getNotications() {
    console.log(this.user.id)
    this.profileUserService.getNotficationsByUser(this.user.sub).subscribe(
      (resp) => {
        this.lisNotification = resp.reverse();
        this.getNumberNotificationNotShowed();
        console.log(this.lisNotification);
      },
      (err) => {
        console.log(err);

      }
    );

  }

  // test
  logout() {
    this.router.navigate(['']);
    this.userStoreService.logout()
  }


  viewNotification(notification: NotficationUser) {
    this.profileUserService.getProjectById(notification.idProject).subscribe(
      (resp) => {
        this.project = resp

        if (this.project.tasks?.length === 0) {
          this.router.navigate(['/new-back-office/dashboard/list-project-without-admin']);
        } else {
          this.router.navigate(['/new-back-office/dashboard/update-project/', notification.idProject]);
        }
      }
    )

    this.profileUserService.sendIdProjectForNotification(notification.idProject);


    this.profileUserService.updateEtatNotificationToShowed(notification.id).subscribe(
      (resp) => {

      },
    );

  }

  getNumberNotificationNotShowed() {
    console.log(this.lisNotification)
    this.lisNotification.forEach(element => {
      if (element.isShowed === true) {
        this.numberNewNotfication++;
      }
    });

  }

  clickedNotification() {
    this.numberNewNotfication = 0;
  }

  approveUpdate(notification: NotficationUser) {
    this.newBackOfficesService.getUser(notification.idCustommer).subscribe(res => {
      this.updateUser = res
      this.UpdateProfile(notification.nameField,notification.updatedValue)
      notification.approved=true;
      this.newBackOfficesService.sendNotficationForApproved(notification,this.user.sub).subscribe(res=>{

      })
    })

  }


  UpdateProfile(nameFieldUpdate: string | undefined, updatedValue: string | undefined){
    switch (nameFieldUpdate) {
      case 'username':
this.updateUser.username=updatedValue;
        break;
      case 'firstName':
        this.updateUser.firstName=updatedValue;

        break;
      case 'email':
        this.updateUser.email=updatedValue;

        break;
      case 'phoneNumber':
        this.updateUser.phoneNumber=updatedValue;

        break;
      case 'company':
        this.updateUser.company=updatedValue;
        break;
    }

    this.profileUserService.updateProfile(this.updateUser).subscribe(
      (resp) => {

      },
    )

  }


}
