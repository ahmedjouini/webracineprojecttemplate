import {Component, OnInit} from '@angular/core';
import {Project} from '../../../shared-module/model/project';
import {MenuItem} from 'primeng/api';
import {UserStoreService} from '../../../authentication/stores/user-store/user-store.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TaskDto} from '../../../common/Task';
import {STATE} from '../../../common/enum/stateTask';
import {CommentDto} from '../../../common/comment-dto';
import {MailContentTask} from '../../../common/mailContentTask-dto';
import {saveAs} from 'file-saver';
import {NewBackOfficeService} from '../../service/new-back-office.service';
import {Subscription} from 'rxjs';
import {ProfileUserService} from '../../../profile-user/service/profile-user.service';

@Component({
  selector: 'app-update-project-admin',
  templateUrl: './update-project-admin.component.html',
  styleUrls: ['./update-project-admin.component.css']
})
export class UpdateProjectAdminComponent implements OnInit {
  accountId: string = '';
  display: boolean = false;
  displayEnable: boolean = false;
  displayUploadFile: boolean = false;
  isDownload: boolean = false;
  listTasks: TaskDto[] = [];
  mailContent: MailContentTask = {}
  listCommentTask: CommentDto[] = [];
  currentTask: TaskDto = {}

  task: TaskDto = {};
  project: Project = {}
  listTypeTasks: any[] = [
    {type: 'Upload'},
    {type: 'Add Comment'},
    {type: 'Send Mail'},
  ]
  first = 0;
  rows = 5;
  items: MenuItem[] = [];
  currentUser: any = {}
  currentIdTask: string = '';

  home: any;
  displayNewTaskPoupup: boolean = false;
  param: string = '';
  idProject: number = 0
  displayTaskEnable: boolean = false;
  displayTaskDelete: boolean = false;
  displayListComments: boolean = false;
  displayContentMail: boolean = false;
  subscriptionIdProjectNotfication?: Subscription ;

  user: any;


  constructor(private servicesBackOfficesService: NewBackOfficeService,
              private userStoreService: UserStoreService,
              private router: Router,
              private route: ActivatedRoute,
              private profileUserService: ProfileUserService,) {
  }

  ngOnInit(): void {
    // @ts-ignore
    this.param = this.route.snapshot.paramMap.get('id');
    this.idProject = +this.param

    this.user = this.userStoreService.parseJWT()

    this.getAllTasks();
    this.getProjectById()
    this.items = [
      {label: 'Task Management'},
      {label: 'List Tasks'},

    ];

    this.home = {icon: 'pi pi-home', routerLink: '/'};
    this.subscriptionIdProjectNotfication = this.profileUserService.getIdProjectForNotification().subscribe(res => {

      this.idProject =res;
      this.getAllTasks();


    })
  }

  getAllTasks() {
    this.servicesBackOfficesService.getAllTasksByProject(this.idProject).subscribe((response) => {

      // @ts-ignore
      response.sort((task1,task2) => task2.id - task1.id);
      this.listTasks=response
    });
    this.display = false;

  }


  getProjectById() {
    this.servicesBackOfficesService.getProject(this.idProject).subscribe(
      res => {
        console.log(res)
      }
    )

  }


  /*
    acceptDelete() {
      this.servicesBackOfficesService.deleteAccountUser(this.accountId).subscribe(
        (response) => {
          this.getAllPrject();
          console.log(response);},
        (error) => {
          this.getAllPrject();
          console.log(error);
        }
      );
    }
  */


  next() {
    this.first = this.first + this.rows;
  }

  prev() {
    this.first = this.first - this.rows;
  }

  reset() {
    this.first = 0;
  }

  isLastPage(): boolean {
    return this.listTasks
      ? this.first === this.listTasks.length - this.rows
      : true;
  }

  isFirstPage(): boolean {
    return this.listTasks ? this.first === 0 : true;
  }


  goDeatail() {
    this.router.navigate(['/profile/etat-project']);
  }

  newTask() {
    this.displayNewTaskPoupup = true;
  }

  createTask() {
    this.task.state = STATE.BLOCKED;
    this.task.enableAccount = false;
    this.displayNewTaskPoupup = false;
    this.addTaskToProject();
    console.log('task created')

  }


  addTaskToProject() {
    this.servicesBackOfficesService.addTaskToProject(this.task, this.idProject , this.user.sub).subscribe((response) => {
      this.project = response;

      console.log(this.project)
      this.getAllTasks();


    }, error => {
      this.getAllTasks();

    });
    this.display = false;

  }

  enableTaskForUser() {

    this.servicesBackOfficesService.enableTaskForUser(this.currentIdTask,this.user.sub,this.idProject).subscribe((response) => {
      this.task = response;
      console.log(this.task)
      this.getAllTasks();

    });
    this.displayTaskEnable = false;

  }

  acceptDeleteTask() {
    this.servicesBackOfficesService.deleteTask(this.currentIdTask, this.idProject).subscribe((response) => {
      console.log(response)
      this.getAllTasks();
      this.displayTaskEnable = false;


    }, error => {
      this.getAllTasks();
      this.displayTaskDelete = false;

    });

  }

  showPoupupEnableTask(id: string) {
    this.displayTaskEnable = true;
    this.currentIdTask = id;
    console.log(this.currentIdTask)


  }

  showPoupupDeleteTask(id: string) {
    this.displayTaskDelete = true;
    this.currentIdTask = id;
  }

  showPoupupListCommentTask(id: string) {
    this.displayListComments = true;
    this.currentIdTask = id;
    this.getAllTaskComment();
  }

  showPoupupMailContent(id: string) {
    this.currentIdTask = id;
    this.getAllMailContent();
    this.displayContentMail = true;


  }

  showPoupupDownoaldFile(task: TaskDto) {
    this.currentTask = task;
    this.displayUploadFile = true;


  }

  getAllTaskComment() {
    this.displayListComments = true
    this.servicesBackOfficesService.getAllCommentsByTasks(this.currentIdTask).subscribe((response) => {
      this.listCommentTask = response;
      console.log('******')
      console.log(this.listCommentTask)

    });
    this.display = false;

  }

  getAllMailContent() {
    this.displayContentMail = true
    this.servicesBackOfficesService.getMailContentBytask(this.currentIdTask).subscribe((response) => {
      this.mailContent = response;
      console.log('******')
      console.log(this.mailContent)

    });
    this.displayContentMail = false;

  }


  downloadFile() {
    console.log(this.currentTask)
    // @ts-ignore
    const file = new Blob([this.currentTask.file.data], {type: 'application/octet-stream'});
    // @ts-ignore
    saveAs(file, this.currentTask.file?.fileName);


  }

  download() {
    this.isDownload = true;
    this.displayUploadFile = false;

    // @ts-ignore
    this.servicesBackOfficesService.downloadFile( ).subscribe(
      response => {

        const file = new Blob([response], {type: 'application/octet-stream'});
        // @ts-ignore
        saveAs(file, this.currentTask.file?.fileName);

      })

  }


}

