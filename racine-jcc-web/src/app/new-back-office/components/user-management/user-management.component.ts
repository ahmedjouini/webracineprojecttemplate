import {Component, Input, OnInit} from '@angular/core';
import {UserDto} from '../../../common/user-dto';
import {NewBackOfficeService} from '../../service/new-back-office.service';
import {Router} from '@angular/router';
import {UserStoreService} from '../../../authentication/stores/user-store/user-store.service';
import {MenuItem, MessageService} from 'primeng/api';
import {Table} from 'primeng/table';
import {Project} from '../../../shared-module/model/project';


// test
@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements OnInit {
  accountId = '';
  currentUser: UserDto={};
  display = false;
  displayEnable = false;
  listProject: Project[]=[];
  first = 0;
  rows = 5;
  user: any;
  home: any;
   @Input() userCameOutSuperMaster:any;
  listUsers: UserDto[] = [];
  listAdmins: UserDto[] = [];
  items: MenuItem[]=[];
  displayUsers= false;
  displayProjects=false;
  popupdeleteUser=false;
  constructor(private newBackOfficesService: NewBackOfficeService,
              private router: Router,private userStoreService: UserStoreService,private messageService: MessageService) { }


  ngOnInit(): void {
    this.user = this.userStoreService.parseJWT()

    if (this.user === undefined) {
      console.log("this.userCameOutSuperMaster")
      console.log(this.userCameOutSuperMaster)
   this.listProject= this.userCameOutSuperMaster.projects
      this.user = this.userCameOutSuperMaster
      this.user.sub = this.userCameOutSuperMaster.id
      this.user.roles[0] = this.userCameOutSuperMaster.roles[0].roleName
    }
    if (this.user.roles[0] === 'MASTER'){
      this.items = [
        {label: 'Users Management'},
        {label: 'My List  Admin'},

      ];
  }else{
      this.items = [
        {label: 'Users Management'},
        {label: 'My List  Customer'},
]
    }
    this.home = {icon: 'pi pi-home', routerLink: '/'};
    this.getAllUser();
  }

  getAllUser(){
    this.newBackOfficesService.getAllUsers(this.user.sub).subscribe(response=>{
      this.listUsers=response
      console.log("this.listUser")
      console.log(this.listUsers)

    })
  }

  enabledUser() {
    this.newBackOfficesService.enableAccountUser(this.accountId).subscribe(
      (response) => {
        this.getAllUser();
        this.displayEnable = false;
this.router.navigate(['new-back-office/dashboard-super-master/list-users'])
      },
      (error) => {

        this.getAllUser();
        this.displayEnable = false;

      }
    );
  }

  showDialogEnableAccount(id: string) {
    this.displayEnable = true;
    this.accountId = id;

  }
  next() {
    this.first = this.first + this.rows;
  }

  prev() {
    this.first = this.first - this.rows;
  }

  reset() {
    this.first = 0;
  }

  isLastPage(): boolean {
    return this.listUsers
      ? this.first === this.listUsers.length - this.rows
      : true;
  }

  isFirstPage(): boolean {
    return this.listUsers ? this.first === 0 : true;
  }

  clear(table: Table) {
    table.clear();
  }

  showDialogUsers(admin: UserDto) {

    this.currentUser=admin;
    this.newBackOfficesService.getAllUsers(admin.id).subscribe(response=>{
      this.listAdmins=response

    })
    this.displayUsers=true;
  }
  showListProject(currentUser: UserDto,customer: UserDto){
    customer.projects?.forEach(appProject => {
      (currentUser.projects?.forEach(project => {

        if (project.id === appProject.id) {
          this.listProject.push(project)
        }


      }))

    })
  }
  showProjects(customer: UserDto) {

    this.listProject=[];
    this.displayProjects=true;
    if(this.user.roles[0]==='ADMIN'){
      this.newBackOfficesService.getUser(this.user.sub).subscribe(res=>{
        this.currentUser=res;
       this. showListProject(this.currentUser,customer)

      })
    }else{
      this. showListProject(this.currentUser,customer)

    }



  }

deleteProject(idProject: number){

    this.newBackOfficesService.deleteProjectFomList(idProject,this.user.sub).subscribe(resp=>{})
  this.getAllUser()
  this.displayProjects=false;
  this.display = false;
  this.displayUsers=false;


}

  showDialog(id: string) {
    this.display = true;
    this.accountId = id;

  }
  showDialogDeleteUserByAdmin(id: string) {
    this.popupdeleteUser = true;
    this.accountId = id;

  }
  deleteUser(){

    console.log(this.accountId)
    this.newBackOfficesService.deleteUser(this.accountId).subscribe(resp=>{

    },error => {
       this.displayUsers=false
      this.display = false;
      this.getAllUser()

      }
    )
  }
deleteUserByAdmin(){
  if (this.user.roles[0] === 'MASTER'){
    this.user.sub=this.currentUser.id

  }
    this.newBackOfficesService.deleteUserByAdmin(this.accountId,this.user.sub).subscribe(res=> {
    } ,error => {
        this.displayUsers=false
        this.popupdeleteUser = false;
        this.getAllUser()
    })
}
  goDetail(id:number) {
    this.router.navigate(['/new-back-office/dashboard/update-project/',id]);

  }

}
