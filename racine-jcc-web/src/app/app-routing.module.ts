import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfirmLinkComponent } from './authentication/component/confirm-Link/confirm-link.component';
import { RegistrationComponent } from './authentication/component/registration/registration.component';
import { SuccessRegistrationComponent } from './authentication/component/success-registration/success-registration.component';
import {ConfirmByPhoneNumberComponent} from './authentication/component/confirm-by-phone-number/confirm-by-phone-number.component';
import {ApplicationGuardGuard} from './shared-module/services/guard/application-guard.guard';
import {ContactRacineComponent} from './dashboard/components/contact-racine/contact-racine.component';
import {ContactComponent} from './dashboard/components/mapRacine/contact.component';
import {MarqueeComponent} from './dashboard/components/marquee/marquee.component';
import {CardComponent} from './dashboard/components/card/card.component';
import {AboutComponent} from './dashboard/components/about/about.component';
import {QuotationComponent} from './dashboard/components/quotation/quotation.component';



const routes: Routes = [

{ path: '', component: RegistrationComponent },

  { path: 'confirmLink/:mail', component: ConfirmLinkComponent },
  { path: 'confirmByPhone/:mail', component:ConfirmByPhoneNumberComponent  },
  {
    path: 'successRegistration/:codeConfirmation',
    component: SuccessRegistrationComponent,
  },  {
    path: 'conatctRacine',
    component: ContactRacineComponent,
  },
  { path: 'mapRacine',
  component: ContactComponent,
},
  { path: 'appquotation',
    component: QuotationComponent,
  },

  { path: 'about',
    component: AboutComponent,
  },
  { path: 'services/:id', component:QuotationComponent  },

  {
    path: 'dashboard',
    loadChildren: () =>
      import('./dashboard/dashboard.module').then(
        (module) => module.DashboardModule
      ),
  },
   {
    path: 'profile',
    loadChildren: () =>
      import('./profile-user/profile-user.module').then(
        (module) => module.ProfileUserModule
      ),
    canActivate: [ApplicationGuardGuard]
  },{
    path: 'new-back-office',
    loadChildren: () =>
      import('./new-back-office/new-back-office.module').then(
        (module) => module.NewBackOfficeModule
      ),


  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
